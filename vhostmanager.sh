#!/bin/bash

#Global
path="/var/www/html"
nameSite=$2
ext=$3
owner=$USER

# Helper - affiche l'aide
help(){
	echo -e "-i : install
		\n-a name_of_website extension : add name_of_website.extension on virtualhosts
		\n-d : delete site.ext"
}

# installer
install(){
	sudo apt update

	sudo apt install apache2 php libapache2-mod-php mariadb-server php-mysql -y

	sudo sed -i "s/Options Indexes FollowSymLinks/Options FollowSymLinks/" /etc/apache2/apache2.conf
	
	sudo service apache2 restart

	sudo mkdir $path 2> /dev/null
		echo -e "$path created"

	sudo chown -R $owner:www-data "/var/www/html"
		echo -e "\nChange owner of /var/www/html to $owner:www-data (Apache)\n"

	sudo chown $owner:www-data /etc/apache2/sites-available/
		echo -e "\nChange owner of /etc/apache2/sites-available/ to $owner:www-data (Apache)\n"

	sudo chmod -R 774 /var/www/html
		echo -e "\nSet /var/www/html permissions with 774\n"

	sudo chown $owner:$owner /etc/hosts
		echo -e "\nChange owner of /etc/hosts to $owner\n"

	sudo a2enmod rewrite
	sudo service apache2 restart
	sudo cp vhostmanager.sh /usr/bin/vhmanager
	sudo chmod 775 /usr/bin/vhmanager
		echo -e "\nNew command has added : vhmanager\nTest vhmanager command with \"vhmanager -h\""
		echo -e `vhmanager -h`
}

# créer site
addSite(){
	prepareWorkSpace
	addToHost
	createApacheConf
	createIndex
	createHtaccess
	sudo chown $owner:www-data -R $path/$nameSite.$ext
	activate
	firefox "http://$nameSite.$ext"
}

prepareWorkSpace(){
	mkdir $path/$nameSite.$ext
	cd $path/$nameSite.$ext
	mkdir db logs web #db : save database / logs : logs apache / web : dossier du site
}

addToHost(){
	echo -e "127.0.0.1\t$nameSite.$ext" >> /etc/hosts
}

createIndex(){
	index="
		\n<!DOCTYPE html>
		\n	<html lang=\"en\">
		\n	<title><?php echo(\$_SERVER)['HTTP_HOST'] ?></title>
		\n	<meta charset=\"UTF-8\">
		\n	<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
		\n	<link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">
		\n	<link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Lato\">
		\n	<link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Montserrat\">
		\n	<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">
		\n	<style>
		\n	body,h1,h2,h3,h4,h5,h6 {font-family: \"Lato\", sans-serif}
		\n	.w3-bar,h1,button {font-family: \"Montserrat\", sans-serif}
		\n	.fa-anchor,.fa-coffee {font-size:200px}
		\n	</style>
		\n	<body>
\n
\n				<!-- Navbar -->
\n				<div class=\"w3-top\">
\n				<div class=\"w3-bar w3-red w3-card w3-left-align w3-large\">
\n					<a class=\"w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-red\" href=\"javascript:void(0);\" onclick=\"myFunction()\" title=\"Toggle Navigation Menu\"><i class=\"fa fa-bars\"></i></a>
\n					<a href=\"/\" class=\"w3-bar-item w3-button w3-padding-large w3-white\"><?php echo(\$_SERVER)['HTTP_HOST'] ?>'s Home</a>
\n					<a href=\"/1\" class=\"w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white\">Try the redirection</a>
\n					<a href=\"/2\" class=\"w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white\">other</a>
\n				</div>
\n
\n				<!-- Navbar on small screens -->
\n				<div id=\"navDemo\" class=\"w3-bar-block w3-white w3-hide w3-hide-large w3-hide-medium w3-large\">
\n					<a href=\"/1\" class=\"w3-bar-item w3-button w3-padding-large\">Try the redirection</a>
\n					<a href=\"/2\" class=\"w3-bar-item w3-button w3-padding-large\">other</a>
\n				</div>
\n				</div>
\n
\n				<!-- Header -->
\n				<header class=\"w3-container w3-red w3-center\" style=\"padding:128px 16px\">
\n				<h1 class=\"w3-margin w3-jumbo\">Hello $owner !</h1>
\n				<p class=\"w3-xlarge\">Template by w3.css</p>
\n				<p>Script by <a href=\"http://busschot.fr\" target=\"_blank\">Sébastien Busschot</a></p>
\n				</header>
\n
\n				<!-- First Grid -->
\n				<div class=\"w3-row-padding w3-padding-64 w3-container\">
\n				<div class=\"w3-content\">
\n					<div class=\"w3-twothird\">
\n					<h1>How to use this structure?</h1>
\n					<h5 class=\"w3-padding-32\">By default, the structure of the website is :
\n						<br>/var/www/html/<?php echo(\$_SERVER)['HTTP_HOST'] ?>/
\n						<br>----------------------------/db => use this for the save of database
\n						<br>----------------------------/logs => each virtualhost use this directory for Apache logs
\n						<br>----------------------------/web/ => your app directory
\n						<br>-----------------------------------/.htaccess
\n						<br>-----------------------------------/index.php
\n					</h5>
\n
\n					<p class=\"w3-text-grey\">The rewrite mod for Apache is enabled by installation script. Now, the redirection is enabled by .htaccess</p>
\n					</div>
\n
\n					<div class=\"w3-third w3-center\">
\n					<i class=\"fa fa-anchor w3-padding-64 w3-text-red\"></i>
\n					</div>
\n				</div>
\n				</div>
\n
\n				<!-- Second Grid -->
\n				<div class=\"w3-row-padding w3-light-grey w3-padding-64 w3-container\">
\n				<div class=\"w3-content\">
\n					<div class=\"w3-third w3-center\">
\n					<i class=\"fa fa-coffee w3-padding-64 w3-text-red w3-margin-right\"></i>
\n					</div>
\n
\n					<div class=\"w3-twothird\">
\n					<h1>Enjoy coding time</h1>
\n					<h5 class=\"w3-padding-32\">Thanks for use this script, he is make by a beginner but he is made whith love <3</h5>
\n
\n					<p class=\"w3-text-grey\">
\n						<ul>Do you want to use PHP framework?
\n							<li><a href=\"https://codeigniter.com/user_guide/installation/index.html\">Codigniter</a>, my favorite, light and powerful. Easy to use.</li>
\n							<li><a href=\"https://symfony.com/doc/current/setup.html\">Symfony</a>, very powerful, he can make coffe. He is more complicated to use.</li>
\n							<li><a href=\"https://laravel.com/docs\">Laravel</a>, very popular, a mix between Codigniter and Symfony.</li>
\n						</ul>
\n					</p>
\n					</div>
\n				</div>
\n				</div>
\n
\n				<div class=\"w3-container w3-black w3-center w3-opacity w3-padding-64\">
\n					<h1 class=\"w3-margin w3-xlarge\">“software is like sex : it's better when it's free..”<br>― Linus Torvalds </h1>
\n				</div>
\n
\n				<!-- Footer -->
\n				<footer class=\"w3-container w3-padding-64 w3-center w3-opacity\">  
\n				<div class=\"w3-xlarge w3-padding-32\">
\n					Script made by : <a href=\"https://www.linkedin.com/in/sebastien-busschot/\" target=\"_blank\"><i class=\"fa fa-linkedin w3-hover-opacity\"></i></a>
\n				</div>
\n				<p>Powered by <a href=\"https://www.w3schools.com/w3css/default.asp\" target=\"_blank\">w3.css</a></p>
\n				</footer>
\n
\n				<script>
\n				// Used to toggle the menu on small screens when clicking on the menu button
\n				function myFunction() {
\n				var x = document.getElementById(\"navDemo\");
\n				if (x.className.indexOf(\"w3-show\") == -1) {
\n					x.className += \" w3-show\";
\n				} else { 
\n					x.className = x.className.replace(\" w3-show\", \"\");
\n				}
\n				}
\n				</script>
\n
\n			</body>
\n		</html>
	"
	echo -e $index > web/index.php
}

createApacheConf(){
	fileContent="<VirtualHost *:80>\n
			#nom de domaine\n
		ServerName $nameSite.$ext \n
			#on accepte aussi le www\n
		ServerAlias www.$nameSite.$ext \n
			#logs d'erreur\n
		ErrorLog $path/$nameSite.$ext/logs/error.log \n
			#logs de connexion\n
		CustomLog $path/$nameSite.$ext/logs/access.log common\n
			#Définition de la racine des sources php\n
		DocumentRoot \"$path/$nameSite.$ext/web/\"\n
		<directory $path/$nameSite.$ext/web/>\n
			Options -Indexes +FollowSymLinks +MultiViews\n
			AllowOverride All\n
			Require all granted\n
			#Header set Access-Control-Allow-Origin \"*\"\n
		</Directory>\n
	</VirtualHost>"

	echo -e $fileContent > /etc/apache2/sites-available/$nameSite.$ext.conf
}

delSite(){
	sudo a2dissite $1.conf
	sudo service apache2 restart
	sudo sed -i "/${1}/d" /etc/hosts
	sudo rm /etc/cron.d/cron_${1}
	sudo rm /etc/apache2/sites-available/${1}.conf

	echo -e "\nVirtualhost supprimé, le dossier contenant le site reste toutefois disponible.\n"
}

createHtaccess(){
	hthaccess="<IfModule mod_rewrite.c>\n
				\t	RewriteEngine On\n
				\t	\n
				\t	# Send would-be 404 requests to Craft\n
				\t	RewriteCond %{REQUEST_FILENAME} !-f\n
				\t	RewriteCond %{REQUEST_FILENAME} !-d\n
				\t	RewriteCond %{REQUEST_URI} $ [NC]\n
				\t	RewriteRule (.+) index.php [QSA,L]\n
				</IfModule>
	"

	echo -e $hthaccess  > web/.htaccess
}

activate(){
	sudo a2ensite $nameSite.$ext.conf
	sudo service apache2 restart
}

database(){
	sudo mysql -e "CREATE DATABASE IF NOT EXISTS ${nameSite}_${ext};"
	mysqldumpQuery="${nameSite}_${ext} > ${path}/${nameSite}.${ext}/db/${nameSite}_${ext}_$(date '+%F').sql"
	echo -e "Add cron runtime\n"
	echo "* * * * 5 root /usr/bin/mysqldump ${mysqldumpQuery}" | sudo tee -a /etc/cron.d/cron_${nameSite}.${ext}
}

case $1 in
	"-i")
		install
	;;
	"-a")
		addSite
		database
	;;
	"-d")
		delSite $2
	;;
	*)
		help
	;;
esac